# Snippets

My snippets.

## Dublin Core, EPUB, SMIL

Made for my Vim-EPUB plugin.

## TEI

Snippets for the Text Encoding Initiative.
